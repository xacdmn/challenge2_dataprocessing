package com.app.menu;

public interface Appearance {

    public void separatorTitle(int size);
    public void header();
    public void footer();
    public void loading();

}
