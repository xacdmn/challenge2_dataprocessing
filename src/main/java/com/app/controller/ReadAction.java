package com.app.controller;

import java.util.List;

public interface ReadAction {
    public List<Integer> readFile(String path);
}
