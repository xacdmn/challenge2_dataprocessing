package com.app.controller;

public interface WriteAction {
    public void writeFreqFinder(String path, String saveFileFreq);
    public void writeMeanMedMod(String path, String saveMeanMedMod);
}
